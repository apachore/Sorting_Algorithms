/*
 * Sorting.c
 *
 *  Created on: Apr 9, 2016
 *      Author: Amit
 */
#include <stdio.h>
#include <stdio.h>
#include <stdbool.h>

void enableFlushAfterPrintf()
{
    setvbuf(stdout, 0, _IONBF, 0);
    setvbuf(stdin, 0, _IONBF, 0);
}

void quicksort(int *, int, int);
void bubblesort(int*,int,int);
void selectionsort(int*,int,int);
void insertionsort(int*,int,int);

int main()
{
    enableFlushAfterPrintf();
    int i;
    int data[] = {10,15,7,0,6,9,7,1,5,44};

    for(i=0;i<10;i++)
    {
        printf("%d ",data[i]);
    }
    printf("\n");

    //quicksort(data,0,10);
    //bubblesort(data,0,10);
    //selectionsort(data,0,10);
    insertionsort(data,0,10);
    for(i=0;i<10;i++)
    {
        printf("%d ",data[i]);
    }
    printf("\n");

    return 0;
}


int partition(int *a, int start, int end)
{

    int pivot = a[(start+end)/2];
    int temp,i,smallindex = start;
    a[(start+end)/2] = a[start];
    a[start] = pivot;
    for(i=start+1;i<end;i++)
    {
        if(a[i]<=pivot)
        {
            smallindex++;
            temp = a[smallindex];
            a[smallindex] = a[i];
            a[i] = temp;
        }
    }
    a[start] =a[smallindex];
    a[smallindex] = pivot;
    return smallindex;
}

void quicksort(int *a, int start, int end)
{
    if(start<end)
    {
        int pIndex = partition(a, start, end);
        quicksort(a,start,pIndex);
        quicksort(a,pIndex+1,end);
    }
}


void bubblesort(int *a, int start, int end)
{
    int i,k,temp;
    bool flag = 0;
    for(k=0;k<end;k++)
    {
        flag = 0;
        for(i=0;i<end-k-1;i++)
        {
            if(a[i]>a[i+1])
            {   flag = 1;
            temp = a[i];
            a[i] = a[i+1];
            a[i+1] = temp;
            }
        }
        if (!flag)
            break;
    }
}

int getMinValueLocation(int *a, int start, int end)
{
    int i;
    int minLocation = start;
    for (i=start+1;i<end;i++)
    {
        if(a[minLocation] >a[i])
        {
            minLocation = i;
        }
    }
    return minLocation;
}

void selectionsort(int*a,int start,int end)
{
    int i,temp,minLocation;
    for(i=0;i<end;i++)
    {
        minLocation = getMinValueLocation(a, i, end);
        temp = a[i];
        a[i] = a[minLocation];
        a[minLocation] = temp;
    }
}

void insertionsort(int*a,int start,int end)
{
    int firstOutofOrder,location;
    int temp;

    for (firstOutofOrder = (start+1);firstOutofOrder <end;firstOutofOrder++)
    {
        if(a[firstOutofOrder]<a[firstOutofOrder-1])
        {
            temp = a[firstOutofOrder];
            location = firstOutofOrder;
            do
            {
                a[location] = a[location-1];
                location--;
            }
            while(a[location-1]>temp && location > 0);
            a[location] = temp;
        }
    }
}
